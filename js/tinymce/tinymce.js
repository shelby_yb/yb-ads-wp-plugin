/*-----------------------------------------------------------------------------------*/
/*  Add TinyMCE Button for Ad Shortcode
/*-----------------------------------------------------------------------------------*/
jQuery(document).ready(function($) {
	tinymce.create('tinymce.plugins.yb_ads_plugin', {
		init : function(ed, url) {
			ed.addButton('yb_ads_button', {
				title : 'Add Ad Slot',
				image : url+'/gallery.png',
				onclick : function() {
					ed.selection.setContent('[yb_ad ad_slot="" ad_page="" ad_display="standard" ad_anim="false" anim_pause="false" new_tab="false"]');
				}
			});
		},
		createControl : function(n, cm) {
			return null;
		},
	});
	tinymce.PluginManager.add('yb_ads_plugin', tinymce.plugins.yb_ads_plugin);
});