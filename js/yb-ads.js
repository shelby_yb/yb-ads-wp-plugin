jQuery(document).ready(function($){
	if($('.ads').length > 0) {
		$('.ads').each(function(adGroupIndex) {
			if ( $(this).children( '.ad' ).length > 0 ) {
				if ($(this).hasClass('fishtank')) {
					$(this).wrap('<div class="fishtank-wrap"></div>');
					var ad_height = $(this).find('img').height();
					var handler = onVisibilityChange($(this).parent('.fishtank-wrap'), ad_height);

					$(window).on('DOMContentLoaded load resize scroll', handler);
				}

				var adImg = $(this).find('img');
				var adSrcs = adImg.data('srcs');
				var adLinks = adImg.data('links');
				var adIDs = adImg.data('ad-ids');
				var adOpenBehaviors = adImg.data('ad-open-behavior');

				// if animation setting chosen, then animate ads
				if( $(this).hasClass( 'anim' ) && ( adSrcs.length > 1 ) ) {
					var $this = $(this);
					adImg.parent('.ad').remove();

					$(adSrcs).each(function(index, val) {
						var adHtml = '<img src="' + val + '" />';
						if (adLinks[index] !== null) {
							adHtml = "<a data-ad-id='" + adIDs[index] + "' style='margin-bottom: 0; display: block; width: 100%; height: 100%;' href='" + adLinks[index] + "' target='" + adOpenBehaviors[index] + "'>" + adHtml +"</a>";
						}

						$this.prepend($('<div class="ad">' + adHtml + '</div>'));
					});
					$this.find('img').show();

					var adGroup = $this;
					var theInt = null;
					var curAd = 1;
					var numAds = adGroup.children('.ad').length;
					var animSpeed = 1000;
					var animPause = parseInt($this.attr('data-anim-pause'), 10) + animSpeed;

					adGroup.children('.ad').css('opacity', '0');
					adGroup.children('.ad:eq(' + (curAd - 1) + ')').addClass('active').css('z-index', '999').css('opacity' , '1');

					var theInterval = function(){
						clearInterval(theInt);

						theInt = setInterval(function(){
							if( curAd === numAds ) {
								curAd = 0;
							}

							adSwitch(curAd, adGroup);
							curAd++;
						}, animPause);
					};

					theInterval();

					adSwitch = function(index, $this) {
						$this.children('.ad.active').animate({opacity: 0}, animSpeed, function() { $(this).css('z-index', '0'); }).removeClass('active');
						$this.children('.ad:eq(' + index + ')').css('z-index', '999').animate({opacity: 1}, animSpeed, function() {
							$(this).addClass('active');
						});
					};
				} else {
					/* Apply random homepage images after the fact, mades images non blocking and circuvents caching */
					var rand = Math.floor(Math.random() * adSrcs.length);

					var adSrcUrl = adSrcs[rand];
					var adLinkUrl = adLinks[rand];
					var adOpenBehavior = adOpenBehaviors[rand];
					var adID = adIDs[rand];

					adImg.attr('src', adSrcUrl).show();

					if (adLinkUrl) {
						adImg.wrap( "<a data-ad-id='" + adID + "' style='margin-bottom: 0; display: block; width: 100%; height: 100%;' href='" + adLinkUrl + "' target='" + adOpenBehavior + "'></a>" );
					}
				}

				// capture the clicks for each ad
				$(this).children( '.ad' ).each(function() {
					$(this).click(function(evt) {
						evt.preventDefault();

						var data = {
							action: 'click_count',
							ad_id: adID
						};

						$.post(MyAjax.ajaxurl, data, function(response) {
							//alert('Got this from the server: ' + response);
						});

						if ( $(this).children( 'a' ).length > 0 ) {
							var adLinkURL = $(this).children( 'a' ).attr( 'href' );
							var $ad_target;
							if ( $(this).children( 'a' ).attr('target') === "_blank" ) {
								$ad_target = '_blank';
							} else {
								$ad_target = '_self';
							}

							window.open(adLinkURL, $ad_target );
						}

						return false;
					});
				});
			}
		});
	}

	function onVisibilityChange (el, ad_height) {
		return function () {
			if(!el.hasClass('fishtank-open')) {
				if(isElementInViewport(el, ad_height)) {
					el.addClass('fishtank-open');
				}
			}
		}
	}

	function isElementInViewport(el, ad_height) {
		var elOffset = el.offset();
		//special bonus for those using jQuery
		if (typeof jQuery === "function" && el instanceof jQuery) {
			el = el[0];
		}

		var rect = el.getBoundingClientRect();
		var bottomOffset = $(document).height() - elOffset.top;

		if ( bottomOffset > ad_height ) {
			bottomOffset = ad_height;
		}

		return (
			rect.top >= 0 &&
			rect.left >= 0 &&
			(rect.top + bottomOffset) <= (window.innerHeight || document.documentElement.clientHeight) && /*or $(window).height() */
			rect.right <= (window.innerWidth || document.documentElement.clientWidth) /*or $(window).width() */
		);
	}
});
