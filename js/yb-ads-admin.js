( function( $ ) {
	$( function() {
		// Set up ad image uploader
		var uploadID = ''; /*setup the var*/
		var adPreview = $('.preview-upload');
		var adURLTest = $('.text-upload');
		var adButton = $('.button-upload');
		var adImgDesc = $('.field-desc');

		$('.button-upload, .preview-upload, .text-upload').click(function() {
			uploadID = $(this).prev('input');
			formfield = $('.upload').attr('name');

			var file_frame;
			// If the media frame already exists, reopen it.
			if ( file_frame ) {
				file_frame.open();
				return;
			}

			// Create the media frame.
			file_frame = wp.media.frames.file_frame = wp.media({
				title: $( this ).data( 'uploader_title' ),
				button: {
					text: $( this ).data( 'uploader_button_text' ),
				},
				multiple: false  // Set to true to allow multiple files to be selected
			});

			// When an image is selected, run a callback.
			file_frame.on( 'select', function() {
				// We set multiple to false so only get one image from the uploader
				attachment = file_frame.state().get('selection').first().toJSON();

				// Do something with attachment.id and/or attachment.url here
				photo_url = attachment['url'];

				adPreview.attr('src', photo_url).show();
				$("<a class='img-delete' href='#'>Delete</a>").insertBefore(adPreview);
				adURLTest.val(photo_url).hide();
				adButton.hide();
				//adImgDesc.hide();
			});

			// Finally, open the modal
			file_frame.open();

			return false;
		});

		$('.img-delete').live("click", function() {
			$(this).remove();
			adPreview.attr('src', '').hide();
			adURLTest.val("").show();
			adButton.show();
			//adImgDesc.show();
			return false;
		});


		// show/hide excludes field for all posts/pages
		$('.post-excludes-wrap').hide();
		$('.all-posts-check').each(function() {
			$thisExcludesField = $(this).parent('td').find('.post-excludes-wrap');
			$thisPostList = $(this).parent('td').find('.post-list-wrap');
			if($(this).is(":checked")) {
				$thisExcludesField.show();
				$thisPostList.hide();
			}
			$(this).click(function() {
				$thisExcludesField = $(this).parent('td').find('.post-excludes-wrap');
				$thisPostList = $(this).parent('td').find('.post-list-wrap');
				if($(this).is(":checked")) {
					$thisExcludesField.show();
					$thisPostList.hide();
				} else {
					$thisExcludesField.hide();
					$thisPostList.show();
				}
			});
		});

		// set up datepicker for scheduling
		$('.datepicker').datepicker({
			dateFormat : 'D, m/d/yy'
		});

	} );


} ( jQuery ) );