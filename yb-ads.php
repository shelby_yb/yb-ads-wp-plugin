<?php
/*
Plugin Name: YB Ads
Plugin URI: http://yellowberri.com
Description: A super minimal plugin for using static ads in wordpress.
Version: 1.05
Author: Shelby Neil Smith
Author URI: http://yellowberri.com
License: GPL2
*/
?>
<?php
/*  Copyright 2013  Shelby Neil Smith  (email : shelby@yellowberri.com)

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License, version 2, as
    published by the Free Software Foundation.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
*/
?>
<?php

//set up hooks for activation & deactivation
function yb_ads_activation() {
}

register_activation_hook(__FILE__, 'yb_ads_activation');

function yb_ads_deactivation() {
}

register_deactivation_hook(__FILE__, 'yb_ads_deactivation');

// class for creating the plugin settings page
///////////////////////////////////////////////
	class yb_ad_settings{
		public function __construct(){
			if( is_admin() ){
				add_action( 'admin_menu', array( $this, 'add_plugin_page' ) );
				add_action( 'admin_init', array( $this, 'page_init' ) );
			}
		}

		public function add_plugin_page(){
			global $yb_ads_settings_page;
			// This page will be under "Settings"
			$yb_ads_settings_page = add_options_page( 'Ads Settings', 'Ads Settings', 'manage_options', 'yb-ads-settings-admin', array( $this, 'create_ads_settings_page' ) );
		}

		public function create_ads_settings_page(){
		?>
			<div class="wrap">
				<?php screen_icon(); ?>
				<h2>Settings</h2>
				<form method="post" action="options.php">
				<?php
					// This prints out all hidden setting fields
					settings_fields( 'yb_ads_option_group' );

					do_settings_sections( 'yb-ads-settings-admin' );
				?>
				<?php submit_button(); ?>
				</form>
			</div>
		<?php
		}

		public function page_init(){
			register_setting( 'yb_ads_option_group', 'yb_ads_options', array( $this, 'check_ID' ) );

			add_settings_section(
				'yb_ads_behavior',
				'Ads Behavior',
				array( $this, 'print_ads_behavior_info' ),
				'yb-ads-settings-admin'
			);

			add_settings_field(
				'yb_ad_random_behavior',
				'Random Behavior:',
				array( $this, 'create_random_behavior_select' ),
				'yb-ads-settings-admin',
				'yb_ads_behavior'
			);
			add_settings_field(
				'yb_ad_anim_pause',
				'Animation Pause (in milliseconds):',
				array( $this, 'create_random_behavior_anim_pause' ),
				'yb-ads-settings-admin',
				'yb_ads_behavior'
			);
			add_settings_field(
				'yb_ad_open_behavior',
				'Ad Open Behavior:',
				array( $this, 'create_ad_open_select' ),
				'yb-ads-settings-admin',
				'yb_ads_behavior'
			);

			add_settings_section(
				'yb_ads_placement_options',
				'Ads Placement Options',
				array( $this, 'print_placement_options_info' ),
				'yb-ads-settings-admin'
			);
			add_settings_field(
				'yb_ad_show_blog_posts',
				'Show Blog Posts:',
				array( $this, 'create_ad_show_blog_checkbox' ),
				'yb-ads-settings-admin',
				'yb_ads_placement_options'
			);
			add_settings_field(
				'yb_ad_show_custom_post_types',
				'Show Custom Post Types:',
				array( $this, 'create_ad_show_cpt_checkbox' ),
				'yb-ads-settings-admin',
				'yb_ads_placement_options'
			);
		}

		public function check_ID( $input ){
			//Define the array for the updated options
			$output = array();

			// Loop through each of the options sanitizing the data
			foreach ($input as $key => $val) {
				if( isset($input[ $key ]) ) {
					$output[ $key ] = strip_tags( stripslashes( $input[$key] ) );
				} // end if
			} // end foreach

			return apply_filters( 'check_ID', $output, $input );
		}

		public function print_ads_behavior_info(){
			print 'Here you can change the way ads appear on the page.';
		}

		public function print_placement_options_info(){
			print 'Settings for what options appear for ad placement.';
		}

		public function create_random_behavior_select(){
			$options = get_option( 'yb_ads_options' );

			$items = array( "Random on page load", "Animated ad slideshow" );
			$html = "<select id='random_behavior' name='yb_ads_options[random_behavior]'>";
			foreach( $items as $item ) {
				if( $options[ 'random_behavior' ] == $item ) {
					$selected = 'selected="selected"';
				} else {
					$selected = '';
				}
				$selected = ( $options[ 'random_behavior' ]==$item ) ? 'selected="selected"' : '';
				$html .= "<option ".$selected." value='".$item."'>$item</option> ";
			}
			$html .= "</select><span class='background-ad-disclaimer'>Will not work with background ads or other special behaviors.</span>";
			echo $html;
		}

		public function create_random_behavior_anim_pause(){
			$options = get_option( 'yb_ads_options' );

			$html = "<input type='text' id='anim_pause' name='yb_ads_options[anim_pause]' value='".$options['anim_pause']."' size='4' />";
			$html .= "<script>";
			$html .= 	"jQuery(function() {";
			$html .= 		"var behav_select = jQuery('#random_behavior');";
			$html .=		"if(behav_select.val() != 'Animated ad slideshow') {";
			$html .= 			"jQuery('input#anim_pause').parent('td').parent('tr').hide();";
			$html .= 			'jQuery(".background-ad-disclaimer").hide();';
			$html .=		"}";
			$html .= 		"behav_select.change(function() {";
			$html .= 			"if(behav_select.val() == 'Animated ad slideshow') {";
			$html .= 				"jQuery('input#anim_pause').parent('td').parent('tr').show();";
			$html .= 				'jQuery(".background-ad-disclaimer").show();';
			$html .=			"} else {";
			$html .= 				"jQuery('input#anim_pause').parent('td').parent('tr').hide();";
			$html .= 				'jQuery(".background-ad-disclaimer").hide();';
			$html .=			"}";
			$html .= 		"});";
			$html .= 	"});";
			$html .= "</script>";
			echo $html;
		}

		public function create_ad_open_select(){
			$options = get_option( 'yb_ads_options' );

			$items = array( "Open all ads in same tab/window", "Open all ads in new tab/window" );
			$html = "<select id='open_behavior' name='yb_ads_options[open_behavior]'>";
			foreach( $items as $item ) {
				if( $options[ 'open_behavior' ] == $item ) {
					$selected = 'selected="selected"';
				} else {
					$selected = '';
				}
				$selected = ( $options[ 'open_behavior' ]==$item ) ? 'selected="selected"' : '';
				$html .= "<option ".$selected." value='".$item."'>$item</option> ";
			}
			$html .= "</select>";
			echo $html;
		}

		public function create_ad_show_blog_checkbox(){
			$options = get_option( 'yb_ads_options' );
			$opt_key = 'show_blog_posts';

			if ( $options[$opt_key] == null ) {
				$options[$opt_key] = 1;
				update_option( 'yb_ads_options', $options );
			}
			$opt_val = isset($options[$opt_key]) ? 1 : 0 ;

			echo '<input type="hidden" name="yb_ads_options['.$opt_key.']" value="0" />';
			printf(
				'<input id="%1$s" name="yb_ads_options[%1$s]" type="checkbox" value="'.$opt_val.'" %2$s />',
				$opt_key,
				checked( (isset($options[$opt_key]) && ($options[$opt_key] != 0)), true, false )
			);
		}
		public function create_ad_show_cpt_checkbox(){
			$options = get_option( 'yb_ads_options' );
			$opt_key = 'show_cpt';

			printf(
				'<input id="%1$s" name="yb_ads_options[%1$s]" type="checkbox" %2$s />',
				$opt_key,
				checked( isset( $options[$opt_key] ), true, false )
			);
		}

	}

	// finally, call the settings class
	new yb_ad_settings();

// enqueue scripts for back and front ends
//////////////////////////////////////////
	add_action( 'admin_enqueue_scripts', 'ybads_admin_scripts' );

	function ybads_admin_scripts( $hook ) {
		global $yb_ads_settings_page;
		if( !is_admin() || ( $hook != 'post.php' && $hook != 'post-new.php' && $hook != $yb_ads_settings_page ) )
			return;
		if( ($hook == 'post.php') || ($hook == 'post-new.php') ) {
			global $post;
			if ( !isset( $post ) || 'yb_ads' != $post->post_type )
				return;
		}
		wp_enqueue_media();
		wp_enqueue_script( 'jquery-ui-datepicker' );
		wp_enqueue_script( 'yb_ads_admin_script', plugins_url( 'js/yb-ads-admin.js', __FILE__ ) );
	}

	add_action('wp_enqueue_scripts', 'ybads_scripts');

	function ybads_scripts() {
		wp_enqueue_script( 'jquery' );

		wp_register_script( 'yb_ads_script', plugins_url( 'js/yb-ads.js', __FILE__ ),array( "jquery" ) );
		wp_enqueue_script( 'yb_ads_script' );
		wp_localize_script( 'yb_ads_script', 'MyAjax', array( 'ajaxurl' => admin_url( 'admin-ajax.php' ) ) );

	}

	function ybads_admin_styles( $hook ) {
		global $yb_ads_settings_page;
		if( !is_admin() || ( $hook != 'post.php' && $hook != 'post-new.php' && $hook != $yb_ads_settings_page ) )
			return;
		if( ($hook == 'post.php') || ($hook == 'post-new.php') ) {
			global $post;
			if ( !isset( $post ) || 'yb_ads' != $post->post_type )
				return;
		}
		wp_register_style( 'yb_ads_admin_css', plugins_url( 'css/yb-ads-admin.css', __FILE__ ) );
		wp_enqueue_style( 'yb_ads_admin_css' );
	}
	add_action( 'admin_enqueue_scripts', 'ybads_admin_styles' );

	function ybads_styles( $hook ) {
		wp_register_style( 'yb_ads_css', plugins_url( 'css/yb-ads.css', __FILE__ ) );
		wp_enqueue_style( 'yb_ads_css' );
	}
	add_action( 'wp_enqueue_scripts', 'ybads_styles' );

// set up taxonomy for ad slots
////////////////////////////////
	add_action( 'init', 'create_ad_slots_taxonomy' );

	function create_ad_slots_taxonomy() {
		// if (!taxonomy_exists('ad_slots')) {
			$ad_slot_labels = array(
				'name' => 'Ad Slots',
				'singular_name' => 'Ad Slot',
				'search_items' => 'Search Ad Slots',
				'all_items' => 'All Ad Slots',
				'parent_item' => 'Parent Ad Slot',
				'parent_item_colon' => 'Parent Ad Slot:',
				'edit_item' => 'Edit Ad Slot',
				'update_item' => 'Update Ad Slot',
				'add_new_item' => 'Add New Ad Slot',
				'new_item_name' => 'New Ad Slot Name',
				'menu_name' => 'Ad Slots',
			);
			$args = array(
				'hierarchical' => true,
				'labels' => $ad_slot_labels,
				'show_ui' => true,
				'show_admin_column' => true,
				'query_var' => 'ad_slots',
				'rewrite' => array( 'slug' => 'ad_slots' ),
			);

			register_taxonomy( 'ad_slots', array( 'yb_ads' ), $args );

			wp_insert_term(
				'Background Takeover', // the term
				'ad_slots', // the taxonomy
				array(
					 'description'=> '',
					 'slug' => 'background-ad',
				)
			);
		// }
	}

	//add extra fields to custom taxonomy edit form
	add_action( 'ad_slots_add_form_fields', 'extra_ad_slot_fields', 10, 2 );
	add_action( 'ad_slots_edit_form_fields', 'extra_ad_slot_fields', 10, 2 );
	function extra_ad_slot_fields( $tag ) {    //check for existing featured ID
		if ( is_object( $tag ) ) {
	 	   $t_id = $tag->term_id;
	 	   $cat_meta = get_option( "category_$t_id" );
	 	} else {
	 		$cat_meta = null;
	 	}
		?>
		<tr class="form-field">
			<th scope="row" valign="top"><label for="ad_slot_width"><?php _e('Ad Slot Width'); ?> <span class="description">(in pixels)</span></label></th>
			<td>
				<input type="text" name="cat_meta[ad_slot_width]" id="cat_meta[ad_slot_width]" size="10" style="width:20%;" value="<?php echo $cat_meta['ad_slot_width'] ? $cat_meta['ad_slot_width'] : ''; ?>"><br />
		    </td>
		</tr>
		<tr class="form-field">
			<th scope="row" valign="top"><label for="ad_slot_height"><?php _e('Ad Slot Height'); ?> <span class="description">(in pixels)</span></label></th>
			<td>
				<input type="text" name="cat_meta[ad_slot_height]" id="cat_meta[ad_slot_height]" size="10" style="width:20%;" value="<?php echo $cat_meta['ad_slot_height'] ? $cat_meta['ad_slot_height'] : ''; ?>"><br />
		    </td>
		</tr>

		<script>
			jQuery(function() {
				//jQuery('#tag-slug').parent('.form-field').hide();
				//jQuery('#slug').parent().parent('.form-field').hide();
				jQuery( '#parent' ).parent( '.form-field' ).hide();
				jQuery( '#parent' ).parent().parent( '.form-field' ).hide();
				jQuery( '#tag-description' ).parent( '.form-field' ).hide();
				jQuery( '#description' ).parent().parent( '.form-field' ).hide();
			});
		</script>
		<?php

	}

	// show width & height columns in ad slots list table
	add_filter( 'manage_edit-ad_slots_columns', 'add_ad_slots_columns' );
	function add_ad_slots_columns( $columns ){
		$columns[ 'width' ] = 'Width';
		$columns[ 'height' ] = 'Height';


		if ( isset($columns[ 'slug' ] ) ) {
			$columns_local[ 'slug' ] = $columns[ 'slug' ];
			unset( $columns[ 'slug' ] );
		}
		if ( isset( $columns[ 'description' ] ) ) {
			$columns_local[ 'description' ] = $columns[ 'description' ];
			unset( $columns[ 'description' ] );
		}
		return $columns;
	}

	add_filter( 'manage_ad_slots_custom_column', 'add_ad_slot_custom_columns',10,3 );
	function add_ad_slot_custom_columns( $content,$column_name,$term_id ){
		$term= get_term( $term_id, 'ad_slots' );
		$t_ID = $term->term_id;
		$term_data = get_option( "category_$t_ID" );

		switch ( $column_name ) {
			case 'width':
				$content = $term_data[ 'ad_slot_width' ];
				break;
			case 'height':
				$content = $term_data[ 'ad_slot_height' ];
				break;
			default:
				break;
		}
		return $content;
	}


	// save extra ad slot fields
	add_action ( 'edited_ad_slots', 'save_extra_ad_slot_fields');
	add_action ( 'created_ad_slots', 'save_extra_ad_slot_fields');
	function save_extra_ad_slot_fields( $term_id ) {
		if ( isset( $_POST[ 'cat_meta' ] ) ) {
			$t_id = $term_id;
			$cat_meta = get_option( "category_$t_id" );
			$cat_keys = array_keys( $_POST[ 'cat_meta' ] );
			foreach ( $cat_keys as $key ){
				if (isset($_POST[ 'cat_meta' ][ $key ])){
					$cat_meta[$key] = $_POST[ 'cat_meta' ][ $key ];
				}
			}
			//save the option array
			update_option( "category_$t_id", $cat_meta );
		}
	}

// register the ad post type
/////////////////////////////
	add_action( 'init', 'yb_ad_init' );
	function yb_ad_init() {
		$labels = array(
			'menu_name' => _x( 'Ads', 'yb_ads' ),
			'name' => 'Ads',
			'singular_name' => 'Ad',
			'add_new' => 'New Ad',
			'add_new_item' => 'Add New Ad',
			'edit_item' => 'Edit Ad',
			'new_item' => 'New Ad',
			'all_items' => 'All Ads',
			'view_item' => 'View Ad',
			'search_items' => 'Search Ads',
			'not_found' =>  'No ads found',
			'not_found_in_trash' => 'No ads found in Trash',
			'parent_item_colon' => '',
			'menu_name' => 'Ads'
		);

		$args = array(
			'labels' => $labels,
			'public' => false,
			'show_ui' => true,
			'show_in_menu' => true,
			'query_var' => true,
			'rewrite' => array( 'slug' => 'ads' ),
			'capability_type' => 'post',
			'exclude_from_search' => true,
			'has_archive' => false,
			'hierarchical' => false,
			'menu_position' => 5,
			'menu_icon' => 'dashicons-images-alt2',
			'supports' => array( 'title' ),
			'register_meta_box_cb' => 'create_ad_post_meta_boxes',
		);

		register_post_type( 'yb_ads', $args );

	}

	function create_ad_post_meta_boxes() {
		add_meta_box('yb_ad_img_meta', 'Ad Image', 'yb_ad_img_meta', 'yb_ads', 'normal', 'high');
		add_meta_box('yb_ad_link_meta', 'Ad Link', 'yb_ad_link_meta', 'yb_ads', 'normal', 'high');
		add_meta_box('yb_ad_placement_meta', 'Ad Placement', 'yb_ad_placement_meta', 'yb_ads', 'normal', 'high');
		add_meta_box('yb_ad_scheduling_meta', 'Ad Scheduling', 'yb_ad_scheduling_meta', 'yb_ads', 'normal', 'high');
	}


	// Ad Image Metabox
	function yb_ad_img_meta() {
		global $post;

		// Noncename needed to verify where the data originated
		echo '<input type="hidden" name="adimgmeta_noncename" id="adimgmeta_noncename" value="' .
		wp_create_nonce( plugin_basename(__FILE__) ) . '" />';

		echo '<table class="form-table">';
		echo '<tr>',
		'<th style="width:20%"><label for="yb_ads_ad_image">Upload Ad Image</label></th>',
		'<td>';

		$ad_img = get_post_meta($post->ID, 'yb_ads_ad_image', true);

		if($ad_img) {
			echo "<a class='img-delete' href='#'>Delete</a>";
			$hidden = "style='display: none;'";
		} else {
			$hidden = "";
		}

		echo "<img style='max-width: 250px; display: block;' src='".$ad_img."' class='preview-upload' />";
		echo "<input ".$hidden." type='text' name='yb_ads_ad_image' class='text-upload' id='yb_ads_ad_image' value='".$ad_img."' />";
		echo "<input ".$hidden." type='button' class='button button-upload' name='_yb_ad_upload_btn' id='_yb_ad_upload_btn' value='Upload' />";

		echo '&nbsp;<span class="field-desc">Be sure to upload an ad image that matches the dimensions of the chosen ad slot.</span>';

		echo '</td></tr>';

		echo '</table>';
	}

	// Ad Link Metabox
	function yb_ad_link_meta() {
		global $post;

		// Noncename needed to verify where the data originated
		echo '<input type="hidden" name="adlinkmeta_noncename" id="adlinkmeta_noncename" value="' .
		wp_create_nonce( plugin_basename(__FILE__) ) . '" />';

		echo '<table class="form-table">';
		echo '<tr>',
		'<th style="width:20%"><label for="yb_ads_ad_link">Ad Link Url</label></th>',
		'<td>';

		$ad_link = get_post_meta($post->ID, 'yb_ads_ad_link', true);

		if($ad_link) {
			$ad_link = 'value="'.$ad_link.'"';
		} else {
			$ad_link = '';
		}

		echo '<input type="text" class="required" name="yb_ads_ad_link" id="yb_ads_ad_link" '.$ad_link.' />';

		echo '</td></tr>';

		echo '</table>';
	}

	// Ad Placement Metabox
	function yb_ad_placement_meta() {
		global $post;

		// Noncename needed to verify where the data originated
		echo '<input type="hidden" name="adplacementmeta_noncename" id="adplacementmeta_noncename" value="' .
		wp_create_nonce( plugin_basename(__FILE__) ) . '" />';

		// Ad Pages Fields
		echo '<table class="form-table">';
		echo '<tr>',
		'<th style="width:20%">Pages</th>',
		'<td>';

		$ad_pages_args = array(
			'sort_order' => 'ASC',
			'sort_column' => 'post_title',
			'post-type' => 'page',
			'post_status' => 'publish',
		);
		$all_pages = get_pages($ad_pages_args);
		$ad_pages = get_post_meta($post->ID, 'yb_ads_page_select');
		$ad_pages = unserialize($ad_pages[0]);

		if(count($all_pages) > 0) {
			if (!empty($ad_pages)) {
				$page_selected = (in_array('all_pages', $ad_pages)) ? 'checked="checked"' : '';
			} else {
				$page_selected = '';
			}
			echo "<input class='all-posts-check' type='checkbox' name='yb_ads_page_select[]' id='yb_ads_page_all' value='all_pages' $page_selected /> <label for='yb_ads_page_all'>All Pages</label><br />";

			$ad_pages_excludes = get_post_meta($post->ID, 'yb_ads_pages_excludes', true);

			if($ad_pages_excludes) {
				$ad_pages_excludes = 'value="'.$ad_pages_excludes.'"';
			} else {
				$ad_pages_excludes = '';
			}
			echo "<div class='post-excludes-wrap'><label for='yb_ads_pages_excludes'>Page Excludes:</label>";
			echo "<input type='text' name='yb_ads_pages_excludes' id='yb_ads_pages_excludes' $ad_pages_excludes /></div>";
			echo "<br />";
			echo "<div class='post-list-wrap'>";
			foreach ( $all_pages as $page ) {
				if (!empty($ad_pages)) {
					$page_selected = (in_array($page->post_name, $ad_pages)) ? 'checked="checked"' : '';
				} else {
					$page_selected = '';
				}
				echo "<input type='checkbox' name='yb_ads_page_select[]' id='yb_ads_page_$page->post_name' value='$page->post_name' $page_selected /> <label for='yb_ads_page_$page->post_name'>$page->post_title</label><br />";
			}
			echo "</div>";
		} else {
			echo "<i>No pages defined.</i>";
		}
		echo '</td></tr>';


		$ad_options = get_option( 'yb_ads_options' );

		if (!empty($ad_options['show_blog_posts']) ) {
			echo '<table class="form-table">';
			echo '<tr>',
			'<th style="width:20%">Blog Posts</th>',
			'<td>';

			// Get all theme taxonomy terms
			$posts_args = array( 'posts_per_page' => -1, 'post_type' => 'post' );
			$all_blog_posts = get_posts($posts_args);

			$key_name = 'yb_ads_posts_select';
			$ad_blog_posts = get_post_meta($post->ID, $key_name);
			$ad_blog_posts = unserialize($ad_blog_posts[0]);

			if(count($all_blog_posts) > 0) {
				if (!empty($ad_blog_posts)) {
					$post_selected = (in_array('all_blog_posts', $ad_blog_posts)) ? 'checked="checked"' : '';
				} else {
					$post_selected = '';
				}
				echo "<input class='all-posts-check' type='checkbox' name='yb_ads_posts_select[]' id='yb_ads_posts_all' value='all_blog_posts' $post_selected /> <label for='yb_ads_posts_all'>All Blog Posts</label><br />";

				$ad_blog_posts_excludes = get_post_meta($post->ID, 'yb_ads_posts_excludes', true);

				if($ad_blog_posts_excludes) {
					$ad_blog_posts_excludes = 'value="'.$ad_blog_posts_excludes.'"';
				} else {
					$ad_blog_posts_excludes = '';
				}
				echo "<div class='post-excludes-wrap'><label for='yb_ads_posts_excludes'>Blog Posts Excludes:</label>";
				echo "<input type='text' name='yb_ads_posts_excludes' id='yb_ads_posts_excludes' $ad_blog_posts_excludes /></div>";
				echo "<br />";
				echo "<div class='post-list-wrap'>";
				foreach ( $all_blog_posts as $blog_post ) {
					if (!empty($ad_blog_posts)) {
						$post_selected = (in_array($blog_post->post_name, $ad_blog_posts)) ? 'checked="checked"' : '';
					} else {
						$post_selected = '';
					}
					echo "<input type='checkbox' name='yb_ads_posts_select[]' id='yb_ads_posts_$blog_post->post_name' value='$blog_post->post_name' $post_selected /> <label for='yb_ads_posts_$blog_post->post_name'>$blog_post->post_title</label><br />";
				}
				echo "</div>";
			} else {
				echo "<i>No posts.</i>";
			}
			echo '</td></tr>';
		}

		if (!empty($ad_options['show_cpt']) ) {
			// Custom Post Types Fields
			$cpt_args = array(
				'public'   => true,
				'_builtin' => false
			);
			$output = 'objects';
			$cpts = get_post_types( $cpt_args, $output );

			foreach ( $cpts as $post_type ) {
				echo '<table class="form-table">';
				echo '<tr>',
				'<th style="width:20%">'.$post_type->label.'</th>',
				'<td>';

				// Get all theme taxonomy terms
				$cpt_args = array( 'posts_per_page' => -1, 'post_type' => $post_type->name );
				$all_cpt_posts = get_posts($cpt_args);

				$key_name = 'yb_ads_cpt_'.$post_type->name.'_select';
				$ad_cpt_posts = get_post_meta($post->ID, $key_name);
				$ad_cpt_posts = unserialize($ad_cpt_posts[0]);
				// print_r($ad_cpt_posts);

				if(count($all_cpt_posts) > 0) {
					if (!empty($ad_cpt_posts)) {
						$post_selected = (in_array('all_'.$post_type->name, $ad_cpt_posts)) ? 'checked="checked"' : '';
					} else {
						$post_selected = '';
					}
					echo "<input class='all-posts-check' type='checkbox' name='yb_ads_cpt_".$post_type->name."_select[]' id='yb_ads_cpt_".$post_type->name."_all' value='all_$post_type->name' $post_selected /> <label for='yb_ads_cpt_".$post_type->name."_all'>All $post_type->label</label><br />";

					$ad_cp_excludes = get_post_meta($post->ID, 'yb_ads_'.$post_type->name.'_excludes', true);

					if($ad_cp_excludes) {
						$ad_cp_excludes = 'value="'.$ad_cp_excludes.'"';
					} else {
						$ad_cp_excludes = '';
					}
					echo "<div class='post-excludes-wrap'><label for='yb_ads_".$post_type->name."_excludes'>".$post_type->label." Excludes:</label>";
					echo "<input type='text' name='yb_ads_".$post_type->name."_excludes' id='yb_ads_".$post_type->name."_excludes' $ad_cp_excludes /></div>";
					echo "<br />";
					echo "<div class='post-list-wrap'>";
					foreach ( $all_cpt_posts as $cpt_post ) {
						if (!empty($ad_cpt_posts)) {
							$post_selected = (in_array($cpt_post->post_name, $ad_cpt_posts)) ? 'checked="checked"' : '';
						} else {
							$post_selected = '';
						}
						echo "<input type='checkbox' name='yb_ads_cpt_".$post_type->name."_select[]' id='yb_ads_cpt_".$post_type->name."_$cpt_post->post_name' value='$cpt_post->post_name' $post_selected /> <label for='yb_ads_cpt_".$post_type->name."_$cpt_post->post_name'>$cpt_post->post_title</label><br />";
					}
					echo "</div>";
				} else {
					echo "<i>No posts.</i>";
				}
				echo '</td></tr>';
			}
		}

		echo '</table>';
	}

	// Ad Scheduling Metabox
	function yb_ad_scheduling_meta() {
		global $post;

		// Noncename needed to verify where the data originated
		echo '<input type="hidden" name="adschedulingmeta_noncename" id="adschedulingmeta_noncename" value="' .
		wp_create_nonce( plugin_basename(__FILE__) ) . '" />';

		echo '<table class="form-table">';
		echo '<tr>',
		'<th style="width:20%"><label for="yb_ads_ad_startdate">Ad Start</label></th>',
		'<td>';

		$ad_start = get_post_meta($post->ID, 'yb_ads_ad_startdate', true);

		if($ad_start) {
			$date = strtotime($ad_start);
			$date = date('D, n/j/Y', $date);
			$ad_start = 'value="'.$date.'"';
		} else {
			$ad_start = '';
		}

		echo '<input type="text" class="datepicker" name="yb_ads_ad_startdate" id="yb_ads_ad_startdate" '.$ad_start.' />';

		echo '</td></tr>';

		// Ad End Date Fields
		echo '<tr>',
		'<th style="width:20%"><label for="yb_ads_ad_enddate">Ad End</label></th>',
		'<td>';

		$ad_end = get_post_meta($post->ID, 'yb_ads_ad_enddate', true);

		if($ad_end) {
			$date = strtotime($ad_end);
			$date = date('D, n/j/Y', $date);
			$ad_end = 'value="'.$date.'"';
		} else {
			$ad_end = '';
		}
		echo '<input type="text" class="datepicker" name="yb_ads_ad_enddate" id="yb_ads_ad_enddate" '.$ad_end.' />';

		echo '</td></tr>';
		echo '</table>';
	}

	// Save the Metabox Data
	function wpt_save_ad_meta($post_id, $post) {

		// verify this came from the our screen and with proper authorization,
		// because save_post can be triggered at other times
		if ( !wp_verify_nonce( $_POST['adimgmeta_noncename'], plugin_basename(__FILE__) )) {
			return $post->ID;
		}
		if ( !wp_verify_nonce( $_POST['adlinkmeta_noncename'], plugin_basename(__FILE__) )) {
			return $post->ID;
		}
		if ( !wp_verify_nonce( $_POST['adplacementmeta_noncename'], plugin_basename(__FILE__) )) {
			return $post->ID;
		}
		if ( !wp_verify_nonce( $_POST['adschedulingmeta_noncename'], plugin_basename(__FILE__) )) {
			return $post->ID;
		}

		// Is the user allowed to edit the post or page?
		if ( !current_user_can( 'edit_post', $post->ID ))
			return $post->ID;

		// OK, we're authenticated: we need to find and save the data
		// We'll put it into an array to make it easier to loop though.

		$ads_meta['yb_ads_ad_image'] = $_POST['yb_ads_ad_image'];
		$ads_meta['yb_ads_ad_link'] = $_POST['yb_ads_ad_link'];
		$ads_meta['yb_ads_page_select'] = serialize($_POST['yb_ads_page_select']);
		$ads_meta['yb_ads_pages_excludes'] = $_POST['yb_ads_pages_excludes'];

		$ad_options = get_option( 'yb_ads_options' );

		if (!empty($ad_options['show_blog_posts']) ) {
			$ads_meta['yb_ads_posts_select'] = serialize($_POST['yb_ads_posts_select']);
			$ads_meta['yb_ads_posts_excludes'] = $_POST['yb_ads_posts_excludes'];
		}

		if (!empty($ad_options['show_cpt']) ) {
			$cpt_args = array(
				'public'   => true,
				'_builtin' => false
			);
			$output = 'objects';
			$cpts = get_post_types( $cpt_args, $output );

			foreach ( $cpts as $post_type ) {
				$post_type_key = 'yb_ads_cpt_'.$post_type->name.'_select';
				$ads_meta[$post_type_key] = serialize($_POST[$post_type_key]);

				$ads_meta['yb_ads_'.$post_type->name.'_excludes'] = $_POST['yb_ads_'.$post_type->name.'_excludes'];
			}
		}

		$ads_meta['yb_ads_ad_startdate'] = $_POST['yb_ads_ad_startdate'];
		$ads_meta['yb_ads_ad_enddate'] = $_POST['yb_ads_ad_enddate'];


		foreach ($ads_meta as $key => $value) { // Cycle through the $events_meta array!
			if( $post->post_type == 'revision' ) return; // Don't store custom data twice

			if ( isset( $_POST[ $key ] ) || isset( $_FILES[ $key ] ) ) {
				$old = get_post_meta( $post->ID, $key, true);

				if ( $value != $old ) {
					if (!in_array('all_pages', $_POST['yb_ads_page_select'])) {
						update_post_meta($post->ID, 'yb_ads_pages_excludes', '');
					}
					$ad_options = get_option( 'yb_ads_options' );
					if (!empty($ad_options['show_blog_posts']) ) {
						if (!in_array('all_blog_posts', $_POST['yb_ads_posts_select'])) {
							update_post_meta($post->ID, 'yb_ads_posts_excludes', '');
						}
					}
					if (!empty($ad_options['show_cpt']) ) {
						foreach ( $cpts as $post_type ) {
							if (!in_array('all_'.$post_type->name, $_POST['yb_ads_cpt_'.$post_type->name.'_select'])) {
								update_post_meta($post->ID, 'yb_ads_'.$post_type->name.'_excludes', '');
							}
						}
					}

					if ( ($key === "yb_ads_ad_startdate") || ($key === "yb_ads_ad_enddate") ) {
						if(!empty($value)) {
							$value = strtotime($value);
							$value = date('Y-m-d', $value);
						} else {
							$date = "";
						}

						update_post_meta($post->ID, $key, $date);
					}

					if(get_post_meta($post->ID, $key, FALSE)) { // If the custom field already has a value
						update_post_meta($post->ID, $key, $value);
					} else { // If the custom field doesn't have a value
						add_post_meta($post->ID, $key, $value, true);
					}
				}
			} else {
				delete_post_meta($post->ID, $key);
			}

		}

		if ( !$_POST[ 'ad_clicks' ][0] ) {
			add_post_meta( $post->ID, 'ad_clicks', '0' );
		}
	}

	// load us jquery for use in validation
	add_action( 'admin_enqueue_scripts-post.php', 'ep_load_jquery_js' );
	add_action( 'admin_enqueue_scripts-post-new.php', 'ep_load_jquery_js' );

	function ep_load_jquery_js(){
		global $post;
		if ( $post->post_type == 'yb_ads' ) {
			wp_enqueue_script( 'jquery' );
		}
	}

add_action('save_post', 'yb_ads_save_post', 10, 2);
function yb_ads_save_post( $ad_id, $ad ) {
	if( defined('DOING_AUTOSAVE') && DOING_AUTOSAVE || $ad->post_type != 'yb_ads') return;

	$errors = array();
	// Validation filters
	$ad_img = $_POST['yb_ads_ad_image'];
	if ( empty( $ad_img ) ) {
		$errors['ad_img'] = "You must choose an ad image.";
	}
	if ( ! $ad->post_title ) {
		$errors['title'] = "The title is required";
	}

	// if we have errors lets setup some messages
	if (! empty($errors)) {
		// we must remove this action or it will loop for ever
		remove_action('save_post', 'yb_ads_save_post');
		// save the errors as option
		update_option('ad_errors', $errors);
		// Change post from published to draft
		$ad->post_status = 'draft';
		// update the post
		wpt_save_ad_meta($ad_id, $ad);
		// we must add back this action
		add_action('save_post', 'yb_ads_save_post');
		// admin_notice is create by a $_GET['message'] with a number that wordpress uses to
		// display the admin message so we will add a filter for replacing default admin message with a redirect
		add_filter( 'redirect_post_location', 'ad_post_redirect_filter' );
	} else {
		wpt_save_ad_meta($ad_id, $ad);
	}
}

function ad_post_redirect_filter( $location ) {
	// remove $_GET['message']
	$location = remove_query_arg( 'message', $location );
	// add our new query sting
	$location = add_query_arg( 'ad', 'error', $location );
	// return the location query string
	return $location;
}
// Add new admin message
add_action( 'admin_notices', 'ad_post_error_admin_message' );
function ad_post_error_admin_message() {
	if ( isset( $_GET['ad'] ) && $_GET['ad'] == 'error' ) {
		// lets get the errors from the option album_errors
		$errors = get_option('ad_errors');
		// now delete the option album errors
		delete_option('ad_errors');
		?>
		<script language="javascript" type="text/javascript">
			jQuery(document).ready(function() {
				jQuery( "#message" ).remove();
				jQuery( "#post" ).children( '.error' ).remove();
				jQuery( "body" ).scrollTop(0);
				<?php
					foreach ( $errors as $error ) {
				?>
					jQuery( "#post" ).prepend( "<div class='error'><p><?php echo $error; ?></p></div>" ).hide().fadeIn();
				<?php
					}
				?>
			});
		</script>
		<?php
	}
}


// Configure ad list screen
//////////////////////////////

	// REMOVE DEFAULT CATEGORY COLUMN
	add_filter( 'manage_yb_ads_posts_columns', 'ads_remove_columns' );
	function ads_remove_columns( $defaults ) {
		// to get defaults column names:
		// print_r( $defaults );
		unset( $defaults[ 'date' ] );
		unset( $defaults[ 'featured' ] );
		unset( $defaults[ 'analytics' ] );
		return $defaults;
	}

	// Add the columns
	add_filter( 'manage_yb_ads_posts_columns', 'new_add_custom_columns', 6 );
	function new_add_custom_columns( $cols ){
		$cols[ 'yb_ads_art' ] = __( 'Ad Image' );
		$cols[ 'yb_ads_clicks' ] = __( 'Clicks' );
		$cols[ 'yb_ads_placement' ] = __( 'Ad Placement' );
		$cols[ 'yb_ads_start' ] = __( 'Ad Start' );
		$cols[ 'yb_ads_end' ] = __( 'Ad End' );
		return $cols;
	}

	// Hook into the posts and pages column managing. Sharing function callback again.
	add_action( 'manage_yb_ads_posts_custom_column', 'new_display_custom_columns', 6, 3 );
	function new_display_custom_columns( $col, $id ){
		$ad_meta = get_post_meta( $id );
		//var_dump($ad_meta);
		switch( $col ){
			case 'yb_ads_art':
				echo "<img style='max-height: 140px; width: auto; max-width: 100%;' src='";
				echo $ad_meta[ 'yb_ads_ad_image' ][0];
				echo "' />";
				break;
			case 'yb_ads_clicks':
				echo $ad_meta[ 'ad_clicks' ][0];
				break;
			case 'yb_ads_placement':
				$ad_page_list = get_post_meta($id, 'yb_ads_page_select');
				$ad_page_list = unserialize($ad_page_list[0]);
				if( $ad_page_list ) {
					if (in_array('all_pages', $ad_page_list)) {
						echo "<strong>All Pages</strong>";
						echo "<br /><br />";
					} else {
						echo "<strong>Pages:</strong><br />";
						foreach( $ad_page_list as $ad_page_item ) {
							$post_obj = get_page_by_path($ad_page_item);
							$title = get_the_title($post_obj->ID);
							echo $title;
							echo "<br />";
						}
						echo "<br />";
					}
				} else {
					// echo "No Pages Selected";
					// echo "<br /><br />";
				}

				$ad_options = get_option( 'yb_ads_options' );

				if (!empty($ad_options['show_blog_posts']) ) {
					$ad_posts_list = get_post_meta($id, 'yb_ads_posts_select');
					$ad_posts_list = unserialize($ad_posts_list[0]);
					if( $ad_posts_list ) {
						if (in_array('all_blog_posts', $ad_posts_list)) {
							echo "<strong>All Blog Posts</strong>";
							echo "<br /><br />";
						} else {
							echo "<strong>Blog Posts:</strong><br />";
							foreach( $ad_posts_list as $ad_posts_item ) {
								$bp_obj = get_page_by_path($ad_posts_item, OBJECT, 'post');
								$bp_title = get_the_title($bp_obj->ID);
								echo $bp_title;
								echo "<br />";
							}
							echo "<br />";
						}
					} else {
						// echo "No ".$post_type->label." Selected";
						// echo "<br /><br />";
					}
				}

				if (!empty($ad_options['show_cpt']) ) {
					$cpt_args = array(
						'public'   => true,
						'_builtin' => false
					);
					$output = 'objects';
					$cpts = get_post_types( $cpt_args, $output );

					foreach ( $cpts as $post_type ) {
						$ad_cpt_list = get_post_meta($id, 'yb_ads_cpt_'.$post_type->name.'_select');
						$ad_cpt_list = unserialize($ad_cpt_list[0]);
						if( $ad_cpt_list ) {
							if (in_array('all_'.$post_type->name, $ad_cpt_list)) {
								echo "<strong>All ".$post_type->label."</strong>";
								echo "<br /><br />";
							} else {
								echo "<strong>$post_type->label:</strong><br />";
								foreach( $ad_cpt_list as $ad_cpt_item ) {
									$cp_obj = get_page_by_path($ad_cpt_item, OBJECT, $post_type->name);
									$cp_title = get_the_title($cp_obj->ID);
									echo $cp_title;
									echo "<br />";
								}
								echo "<br />";
							}
						} else {
							// echo "No ".$post_type->label." Selected";
							// echo "<br /><br />";
						}
					}
				}
				break;
			case 'yb_ads_start':
				if( isset( $ad_meta[ 'yb_ads_ad_startdate' ][0] ) && strlen( $ad_meta[ 'yb_ads_ad_startdate' ][0] ) > 0 ) {
					$start_date = strtotime( $ad_meta[ 'yb_ads_ad_startdate' ][0] );
					$start_date = date( 'D, n/j/Y', $start_date );
				} else {
					$start_date = "";
				}
				echo $start_date;
				break;
			case 'yb_ads_end':
				if( isset( $ad_meta[ 'yb_ads_ad_enddate' ][0] ) && strlen( $ad_meta[ 'yb_ads_ad_enddate' ][0] ) > 0 ) {
					$end_date = strtotime( $ad_meta[ 'yb_ads_ad_enddate' ][0] );
					$end_date = date( 'D, n/j/Y', $end_date );
				} else {
					$end_date = "";
				}
				echo $end_date;
				break;
		}
	}

// Set up functions to sort ads by fields in the back-end
/////////////////////////////////////////////////////////

	// get page filter options
	add_action( 'restrict_manage_posts', 'wpse45436_admin_posts_placement_filter_restrict_manage_posts' );
	function wpse45436_admin_posts_placement_filter_restrict_manage_posts(){
		$type = 'post';
		if ( isset( $_GET[ 'post_type' ] ) ) {
			$type = $_GET[ 'post_type' ];
		}

		if ( 'yb_ads' == $type ){
			?>
			<select name="ADMIN_PLACEMENT_FILTER_FIELD_VALUE">
	 			<option value=""><?php _e( 'Filter By Post Placement', 'wose45436' ); ?></option>
	 			<?php
					$current_v = isset($_GET[ 'ADMIN_PLACEMENT_FILTER_FIELD_VALUE' ]) ? urldecode($_GET[ 'ADMIN_PLACEMENT_FILTER_FIELD_VALUE' ]) : '';
 					echo "<optgroup label='Pages'>";

	 				$page_args = array( 'post_status' => 'publish', );
					$pg_options = get_pages( $page_args );

					foreach ($pg_options as $pl_post) {
						$value = serialize(array('pages', $pl_post->post_name));
						$label = $pl_post->post_title;

						if ($value == $current_v) {
							$selected = " selected='selected'";
						} else {
							$selected = "";
						}

						echo "<option value='".urlencode($value)."'$selected>$label</option>";
					}
	 				echo "</optgroup>";

					$ad_options = get_option( 'yb_ads_options' );

					if (!empty($ad_options['show_blog_posts']) ) {
	 					echo "<optgroup label='Blog Posts'>";

		 				$bp_args = array( 'post_type' => 'post', 'posts_per_page' => -1, 'post_status' => 'publish', 'orderby' => 'title', 'order' => 'ASC', );

						$bp_options = get_posts( $bp_args );

						foreach ($bp_options as $pl_post) {
							$value = serialize(array('blog_posts', $pl_post->post_name));
							$label = $pl_post->post_title;

							if ($value == $current_v) {
								$selected = " selected='selected'";
							} else {
								$selected = "";
							}

							echo "<option value='".urlencode($value)."'$selected>$label</option>";
						}
		 				echo "</optgroup>";
					}

					if (!empty($ad_options['show_cpt']) ) {
						$cpt_args = array(
							'public'   => true,
							'_builtin' => false
						);
						$output = 'objects';
						$cpts = get_post_types( $cpt_args, $output );

						foreach ( $cpts as $cp_type ) {
		 					echo "<optgroup label='".$cp_type->label."'>";

			 				$cp_args = array( 'post_type' => $cp_type->name, 'posts_per_page' => -1, 'post_status' => 'publish', 'orderby' => 'title', 'order' => 'ASC', );

							$cp_options = get_posts( $cp_args );

							foreach ($cp_options as $pl_post) {
								$value = serialize(array($cp_type->name, $pl_post->post_name));
								$label = $pl_post->post_title;

								if ($value == $current_v) {
									$selected = " selected='selected'";
								} else {
									$selected = "";
								}

								echo "<option value='".urlencode($value)."'$selected>$label</option>";
							}

		 					echo "</optgroup>";
						}
					}
				?>
	 		</select>
	 		<?php
		}
	}

	// get slot filter options
	add_action( 'restrict_manage_posts', 'wpse45436_admin_posts_slot_filter_restrict_manage_posts' );
	function wpse45436_admin_posts_slot_filter_restrict_manage_posts(){
		$type = 'post';
		if ( isset( $_GET[ 'post_type' ] ) ) {
			$type = $_GET[ 'post_type' ];
		}

		if ('yb_ads' == $type){

			$slot_terms = get_terms( 'ad_slots', 'hide_empty=0' );
			?>
				<select name="ADMIN_SLOT_FILTER_FIELD_VALUE">
				<option value=""><?php _e( 'Filter By Slot', 'wose45436' ); ?></option>
				<?php
					$current_v = isset( $_GET[ 'ADMIN_SLOT_FILTER_FIELD_VALUE' ] )? $_GET[ 'ADMIN_SLOT_FILTER_FIELD_VALUE' ]:'';
					foreach ($slot_terms as $slot_term) {
						$value = $slot_term->slug;
						$label = $slot_term->name;
						printf
						(
							'<option value="%s"%s>%s</option>',
							$value,
							$value == $current_v? ' selected="selected"':'',
							$label
						);
					}
				?>
			</select>
			<?php
		}
	}

	// then display filtered results
	add_filter( 'parse_query', 'wpse45436_posts_filter' );
	function wpse45436_posts_filter( $query ){
		global $pagenow;

		if ( ( $query->query['post_type'] == 'yb_ads' ) && is_admin() && ( $pagenow == 'edit.php' ) && ( ( isset( $_GET[ 'ADMIN_PLACEMENT_FILTER_FIELD_VALUE' ] ) && $_GET[ 'ADMIN_PLACEMENT_FILTER_FIELD_VALUE' ] != '' ) || ( isset( $_GET[ 'ADMIN_SLOT_FILTER_FIELD_VALUE' ] ) && $_GET[ 'ADMIN_SLOT_FILTER_FIELD_VALUE' ] != '' ) ) ) {
			$qv = &$query->query_vars;
			$qv['meta_query'] = array();

			if( isset( $_GET[ 'ADMIN_PLACEMENT_FILTER_FIELD_VALUE' ] ) && strlen( $_GET[ 'ADMIN_PLACEMENT_FILTER_FIELD_VALUE' ] ) > 0) {
				$value_arr = unserialize(urldecode($_GET[ 'ADMIN_PLACEMENT_FILTER_FIELD_VALUE' ]));

				if ($value_arr[0] == 'pages') {
					$qv[ 'meta_query' ][] = array(
						'key' => 'yb_ads_page_select',
						'value'    => 'all_pages',
						'compare'    => 'LIKE'
					);
				}
				$qv[ 'meta_query' ][] = array(
					'key' => 'yb_ads_page_select',
					'value'    => $value_arr[1],
					'compare'    => 'LIKE'
				);

				$ad_options = get_option( 'yb_ads_options' );

				if (!empty($ad_options['show_blog_posts']) ) {
					if ($value_arr[0] == 'blog_posts') {
						$qv[ 'meta_query' ][] = array(
							'key' => 'yb_ads_posts_select',
							'value'    => 'all_blog_posts',
							'compare'    => 'LIKE'
						);
					}
					$qv[ 'meta_query' ][] = array(
						'key' => 'yb_ads_posts_select',
						'value'    => $value_arr[1],
						'compare'    => 'LIKE'
					);
				}

				if (!empty($ad_options['show_cpt']) ) {
					$cpt_args = array(
						'public'   => true,
						'_builtin' => false
					);
					$output = 'objects';
					$cpts = get_post_types( $cpt_args, $output );

					foreach ( $cpts as $post_type ) {
						if ($value_arr[0] == $post_type->name) {
							$qv[ 'meta_query' ][] = array(
								'key' => 'yb_ads_cpt_'.$post_type->name.'_select',
								'value'    => 'all_'.$post_type->name,
								'compare'    => 'LIKE'
							);
						}
						$qv[ 'meta_query' ][] = array(
							'key' => 'yb_ads_cpt_'.$post_type->name.'_select',
							'value'    => $value_arr[1],
							'compare'    => 'LIKE'
						);
					}
				}

				$qv[ 'meta_query' ][ 'relation' ] = 'OR';
			}


			if( isset( $_GET[ 'ADMIN_SLOT_FILTER_FIELD_VALUE' ] ) && strlen( $_GET[ 'ADMIN_SLOT_FILTER_FIELD_VALUE' ] ) > 0 ) {
				$qv[ 'tax_query' ] [] = array(
					'taxonomy' => 'ad_slots',
					'field'    => 'slug',
					'terms'    => $_GET[ 'ADMIN_SLOT_FILTER_FIELD_VALUE' ]
				);
			}


		}
	}


	// tally click count!
	add_action( 'wp_ajax_nopriv_click_count', 'ad_click_count' );
	add_action( 'wp_ajax_click_count', 'ad_click_count' );
	function ad_click_count() {
		$postID = $_POST[ 'ad_id' ];
		$adPostMeta = get_post_meta( $postID );
		$curClickCount = intval($adPostMeta[ 'ad_clicks' ][0]);

		$newClickCount = $curClickCount + 1;
		update_post_meta( $postID, 'ad_clicks', $newClickCount );

		exit;
	}


//create the user function
//////////////////////////
	// $ad_display = "standard", "fishtank", "background"
	function yb_ads($ad_slot, $ad_page="", $ad_display="standard", $ad_anim=null, $anim_pause=false, $new_tab=false) {
		wp_reset_postdata();
		wp_reset_query();

		global $post;

		$ad_options = get_option( 'yb_ads_options' );

		$random_behavior = $ad_options[ 'random_behavior' ];

		if ( ( $ad_anim === "true" ) || ( $ad_anim === true ))  {
			$random_behavior = "anim";
			if ( !$anim_pause || ($anim_pause === "false") ) {
				$anim_pause = 4000;
			}
		} else if ( ( $ad_anim === "false" ) || ( $ad_anim === false ) ) {
			if ( $random_behavior == "Random on page load" ) {
				$random_behavior = "no-anim";
				$anim_pause = 0;
			} elseif ( $random_behavior == "Animated ad slideshow" ) {
				$random_behavior = "anim";
				$anim_pause = $ad_options[ 'anim_pause' ];
			} else {
				$random_behavior = "";
				$anim_pause = 0;
			}
		} else {
			$random_behavior = "";
			$anim_pause = 0;
		}

		// get the post type of the current post, to have for later
		$cur_post_type = get_post_type( $post );

		if(empty($ad_page)) {
			if (empty( $post->post_parent )) {
				$ad_page_obj = get_post( $post->ID );
			} else {
				$ad_page_obj = get_post( $post->post_parent );
			}
			$ad_page = $ad_page_obj->post_name;
		}

		$ad_slot = str_replace(" ", "-", strtolower($ad_slot));
		$current = date( 'Y-m-d' );

		$ad_args = array(
			'post_type' => 'yb_ads',
			'tax_query' => array(
				array(
					'taxonomy' => 'ad_slots',
					'field' => 'slug',
					'terms' => $ad_slot
				)
			)
		);
		$slot_ads = new WP_Query($ad_args);

		if($slot_ads->have_posts()) :
			$term = get_term_by( 'slug', $ad_slot, 'ad_slots' );
			$t_ID = $term->term_id;
			$term_data = get_option( "category_$t_ID" );
			$ad_html = "";
			$ad_index = 0;
			$ad_srcs = [];
			$ad_links = [];
			$ad_open_behaviors = [];
			$ad_ids = [];
			$ad_open_global = $ad_options[ 'open_behavior' ];

			if($ad_display === "background") {
				$ad_html .= "<div class='background-takeover-ad ads' class='ads ad-page-".str_replace(" ", "-", strtolower($ad_page))." ad-slot-".str_replace(" ", "-", strtolower($ad_slot))." ".$random_behavior."' data-anim-pause='".$anim_pause."'>";
			} else {
				$ad_slot_width = $term_data[ 'ad_slot_width' ];
				$ad_slot_height = $term_data[ 'ad_slot_height' ];
				$ad_slot_aspect_ratio = ( $ad_slot_height / $ad_slot_width ) * 100;

				$ad_html .= "<div style='padding-bottom: ".$ad_slot_aspect_ratio."%;' class='ads ad-page-".str_replace(" ", "-", strtolower($ad_page))." ad-slot-".str_replace(" ", "-", strtolower($ad_slot))." ".$random_behavior." ".$ad_display."' data-anim-pause='".$anim_pause."'>";
			}

			while($slot_ads->have_posts()) : $slot_ads->the_post();

				$ads_meta = get_post_meta(get_the_ID());

				$ad_placement = [];

				$ad_pages = get_post_meta(get_the_ID(), 'yb_ads_page_select');
				$ad_pages = unserialize($ad_pages[0]);

				$ad_pages_excludes = split(',', get_post_meta($post->ID, 'yb_ads_pages_excludes', true));

				if (is_array($ad_pages) || is_object($ad_pages)) {
					foreach($ad_pages as $page) {
						if (!in_array($page, $ad_pages_excludes)) {
							$ad_placement[] = $page;
						}
					}
				} else {
					if (!in_array($ad_pages, $ad_pages_excludes)) {
						$ad_placement[] = $ad_pages;
					}
				}
				if (!empty($ad_options['show_blog_posts']) ) {
					$ad_blog_posts = get_post_meta(get_the_ID(), 'yb_ads_posts_select');
					$ad_blog_posts = unserialize($ad_blog_posts[0]);

					$ad_bp_excludes = split(',', get_post_meta($post->ID, 'yb_ads_posts_excludes', true));

					if (is_array($ad_blog_posts) || is_object($ad_blog_posts)) {
						foreach($ad_blog_posts as $blog_post) {
							if (!in_array($blog_post, $ad_bp_excludes)) {
								$ad_placement[] = $blog_post;
							}
						}
					} else {
						if (!in_array($ad_blog_posts, $ad_bp_excludes)) {
							$ad_placement[] = $ad_blog_posts;
						}
					}
				}
				if (!empty($ad_options['show_cpt']) ) {
					$cpt_args = array(
						'public'   => true,
						'_builtin' => false
					);
					$output = 'objects';
					$cpts = get_post_types( $cpt_args, $output );

					foreach ( $cpts as $post_type ) {
						$ad_cpt_posts = get_post_meta(get_the_ID(), 'yb_ads_cpt_'.$post_type->name.'_select');
						$ad_cpt_posts = unserialize($ad_cpt_posts[0]);

						$ad_cp_excludes[$post_type->name] = split(',', get_post_meta($post->ID, 'yb_ads_'.$post_type->name.'_excludes', true));

						if (is_array($ad_cpt_posts) || is_object($ad_cpt_posts)) {
							foreach($ad_cpt_posts as $cpt_post) {
								if (!in_array($cpt_post, $ad_cp_excludes[$post_type->name])) {
									$ad_placement[] = $cpt_post;
								}
							}
						} else {
							if (!in_array($ad_cpt_posts, $ad_cp_excludes[$post_type->name])) {
								$ad_placement[] = $ad_cpt_posts;
							}
						}
					}
				}

				$ad_this_post = false;

				if (($cur_post_type == 'page') && (in_array('all_pages', $ad_placement))) {
					if (!in_array($ad_page, $ad_pages_excludes)) {
						$ad_this_post = true;
					}
				}
				if (!empty($ad_options['show_blog_posts']) ) {
					if (($cur_post_type == 'post') && (in_array('all_blog_posts', $ad_placement))) {
						if (!in_array($ad_page, $ad_bp_excludes)) {
							$ad_this_post = true;
						}
					}
				}
				if (!empty($ad_options['show_cpt']) ) {
					$cpt_args = array(
						'public'   => true,
						'_builtin' => false
					);
					$output = 'objects';
					$cpts = get_post_types( $cpt_args, $output );

					foreach ( $cpts as $post_type ) {
						if (($cur_post_type == $post_type->name) && (in_array('all_'.$post_type->name, $ad_placement))) {
							if (!in_array($ad_page, $ad_cp_excludes[$post_type->name])) {
								$ad_this_post = true;
							}
						}
					}
				}

				if (in_array($ad_page, $ad_placement)) {
					$ad_this_post = true;
				}

				if ($ad_this_post) {
					if (
						( !$ads_meta[ 'yb_ads_ad_startdate' ][0] && !$ads_meta[ 'yb_ads_ad_enddate' ][0]) ||
						(( $ads_meta[ 'yb_ads_ad_startdate' ][0] && !$ads_meta[ 'yb_ads_ad_enddate' ][0]) && ($current >= $ads_meta[ 'yb_ads_ad_startdate' ][0])) ||
						(( !$ads_meta[ 'yb_ads_ad_startdate' ][0] && $ads_meta[ 'yb_ads_ad_enddate' ][0]) && ($current <= $ads_meta[ 'yb_ads_ad_enddate' ][0])) ||
						(( $current >= $ads_meta[ 'yb_ads_ad_startdate' ][0]) && ($current <= $ads_meta[ 'yb_ads_ad_enddate' ][0]))) {

						$ad_ids[$ad_index] = $post->ID;


						// check for ad link and add http(s), if necessary
						if ( !empty( $ads_meta[ 'yb_ads_ad_link' ][0] ) ) {
							$ad_links[$ad_index] = preg_replace('/^(?!https?:\/\/)/', 'http://', $ads_meta[ 'yb_ads_ad_link' ][0]);

							// get ad link open behavior for this ad
							if ( $new_tab || $ad_open_global == "Open all ads in new tab/window" ) {
								$ad_open_behaviors[$ad_index] =  "_blank";
							} else {
								$ad_open_behaviors[$ad_index] = "_self";
							}
						} else {
							$ad_links[$ad_index] = null;
							$ad_open_behaviors[$ad_index] = null;
						}

						$ad_srcs[$ad_index] = $ads_meta[ 'yb_ads_ad_image' ][0];
						$ad_index++;
					}
				}
			endwhile;

			$srcsJson = json_encode($ad_srcs);
			$linksJson = json_encode($ad_links);
			$adOpenBehaviorsJson = json_encode($ad_open_behaviors);
			$idsJson = json_encode($ad_ids);

			$ad_html .= "<div class='ad'>";
			$ad_html .= "<img data-total='$total_srcs' data-ad-open-behavior='$adOpenBehaviorsJson' data-ad-ids='$idsJson' data-srcs='$srcsJson' data-links='$linksJson' src='$ad_srcs[0]' />";
			$ad_html .= "</div></div>";

			wp_reset_postdata();
			wp_reset_query();

			if ( $ad_index > 0 ) {
				return $ad_html;
			} else {
				return false;
			}
		endif;
	}

// create shortcode and button for tinymce
//////////////////////////
	// first, let's create the shortcode
	add_shortcode('yb_ad', 'yb_ads_shortcode');
	function yb_ads_shortcode( $atts ) {
		extract( shortcode_atts( array(
			'ad_slot' => '',
			'ad_page' => '',
			'ad_display' => 'standard',
			'ad_anim' => 'false',
			'anim_pause' => 'false',
			'new_tab' => 'false'
		), $atts ) );

		return yb_ads($ad_slot, $ad_page, $ad_display, $ad_anim, $anim_pause, $new_tab);
	}

	// init process for registering our button
	add_action('init', 'yb_ads_shortcode_button_init');
	function yb_ads_shortcode_button_init() {
		//Abort early if the user will never see TinyMCE
		// if ( ! current_user_can('edit_posts') && ! current_user_can('edit_pages') && get_user_option('rich_editing') == 'true')
		// 	return;

		add_filter("mce_external_plugins", "yb_ads_add_button");
		add_filter('mce_buttons', 'yb_ads_register_button');
	}

	function yb_ads_add_button($plugin_array) {
		$plugin_array['yb_ads_plugin'] = plugin_dir_url( __FILE__ ) . 'js/tinymce/tinymce.js';
		return $plugin_array;
	}

	function yb_ads_register_button($buttons) {
		array_push( $buttons, 'yb_ads_button' );
		return $buttons;
	}
?>